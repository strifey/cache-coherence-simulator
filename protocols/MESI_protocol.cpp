#include "MESI_protocol.h"
#include "../sim/mreq.h"
#include "../sim/sim.h"
#include "../sim/hash_table.h"

extern Simulator *Sim;

#define D (0)

/*************************
 * Constructor/Destructor.
 *************************/
MESI_protocol::MESI_protocol (Hash_table *my_table, Hash_entry *my_entry)
    : Protocol (my_table, my_entry)
{
    this->state = MESI_CACHE_I;
}

MESI_protocol::~MESI_protocol ()
{    
}

void MESI_protocol::dump (void)
{
    const char *block_states[7] = {"X","I","S","E","M","IS","IM"};
    fprintf (stderr, "MESI_protocol - state: %s\n", block_states[state]);
}

/*
 * Handle proc request for data based on current state. Make requests
 * to BUS if necessary and handle any state update according to current state
 */
void MESI_protocol::process_cache_request (Mreq *request)
{
	switch (state) {
		case MESI_CACHE_I:  if(D)printf("cI\n");do_cache_I (request); break;
		case MESI_CACHE_IS:  if(D)printf("cIS\n");do_cache_IT (request); break;
		case MESI_CACHE_S:  if(D)printf("cS\n");do_cache_S (request); break;
		case MESI_CACHE_M:  if(D)printf("cM\n");do_cache_M (request); break;
		case MESI_CACHE_IM: if(D)printf("cIM\n");do_cache_IT (request); break;
		case MESI_CACHE_SM: if(D)printf("cSM\n");do_cache_IT (request); break;
		case MESI_CACHE_E: if(D)printf("cE\n");do_cache_E (request); break;
    default: fatal_error ("Invalid Cache State for MESI Protocol\n");
    }
}

/*
 * Based on current state, call function associated with state
 * and perform any snooping of BUS or state updates required by this state
 * based on bus information
 */
void MESI_protocol::process_snoop_request (Mreq *request)
{
	switch (state) {
		case MESI_CACHE_I: if(D)printf("sI\n"); do_snoop_I (request); break;
		case MESI_CACHE_IS: if(D)printf("sIS\n"); do_snoop_IS (request); break;
		case MESI_CACHE_S:  if(D)printf("sS\n");do_snoop_S (request); break;
		case MESI_CACHE_M:  if(D)printf("sM\n");do_snoop_M (request); break;
		case MESI_CACHE_IM: if(D)printf("sIM\n");do_snoop_IM (request); break;
		case MESI_CACHE_SM: if(D)printf("sIM\n");do_snoop_SM (request); break;
		case MESI_CACHE_E: if(D)printf("sE\n");do_snoop_E (request); break;
    default: fatal_error ("Invalid Cache State for MESI Protocol\n");
    }
}

/*
 * We're invalid, so request the data from BUS no matter what
 */
inline void MESI_protocol::do_cache_I (Mreq *request)
{
	switch (request->msg) {
		case LOAD:
			send_GETS(request->addr);
			state = MESI_CACHE_IS;
			Sim->cache_misses++;
			break;
		case STORE:
			send_GETM(request->addr);
			state = MESI_CACHE_IM;
			Sim->cache_misses++;
			break;
		default:
			request->print_msg (my_table->moduleID, "ERROR");
			fatal_error ("Client: I state shouldn't see this message\n");
	}
}

/*Dummy do_cache taht should never happen since I should be waiting on 
 * a response from BUS first
 */
inline void MESI_protocol::do_cache_IT (Mreq *request)
{
	switch (request->msg) {
		case LOAD:
		case STORE:
			request->print_msg (my_table->moduleID, "ERROR");
			fatal_error("Should only have one outstanding request per processor!");
		default:
			request->print_msg (my_table->moduleID, "ERROR");
			fatal_error ("Client: IT state shouldn't see this message\n");
	}
}

/*
 * Supply data on read. On write, GETM and then 
 * wait until you see GETM in SM state to supply data to 
 * self. Must do this to let other caches know you
 * modified data
 */
inline void MESI_protocol::do_cache_S (Mreq *request)
{
	switch (request->msg) {
		case STORE:
			//Invalidate other caches
			send_GETM(request->addr);
			state = MESI_CACHE_SM;
			Sim->cache_misses++;
			break;
		case LOAD:
			send_DATA_to_proc(request->addr);
			break;
		default:
			request->print_msg (my_table->moduleID, "ERROR");
			fatal_error ("Client: S state shouldn't see this message\n");
	}
}

/*
 * Perform silent upgrade on write or just supply data to proc on read
 */
inline void MESI_protocol::do_cache_E (Mreq *request)
{
	switch (request->msg) {
		case STORE:
			Sim->silent_upgrades++;
			state = MESI_CACHE_M;
		case LOAD:
			send_DATA_to_proc(request->addr);
			break;
		default:
			request->print_msg (my_table->moduleID, "ERROR");
			fatal_error ("Client: E state shouldn't see this message\n");
	}
}

/*Since we're in M, supply data to proc no matter what.
 * WE dah boss.
 */
inline void MESI_protocol::do_cache_M (Mreq *request)
{
	switch (request->msg) {
		case LOAD:
		case STORE:
			send_DATA_to_proc(request->addr);
			break;
		default:
			request->print_msg (my_table->moduleID, "ERROR");
			fatal_error ("Client: M state shouldn't see this message\n");
	}
}

/*
 * Snoops for I and does nothing, since it's I.
 * Boring snoop
 */
inline void MESI_protocol::do_snoop_I (Mreq *request)
{
	switch (request->msg) {
		case GETS:
		case GETM:
		case DATA:
			break;
		default:
			request->print_msg (my_table->moduleID, "ERROR");
			fatal_error ("Client: I state shouldn't see this message\n");
	}
}

/*Handles getting data from GETS on invalid cache line
 */
inline void MESI_protocol::do_snoop_IS (Mreq *request)
{
	switch (request->msg) {
		case GETM: //WUT WHY
		case GETS:
			break; //MY OWN
		case DATA:
			if (!get_shared_line()){
				state = MESI_CACHE_E;
				//printf("put something in E! %lu addr: \n", Global_Clock);
			}
			else
				state = MESI_CACHE_S;
			send_DATA_to_proc(request->addr);
			break;
		default:
			request->print_msg (my_table->moduleID, "ERROR");
			fatal_error ("Client: IS state shouldn't see this message\n");
	}
}

/*
 * Checks line and sets that shared to let others now a cache does have this
 * data
 * Invalidates on GETM
 */
inline void MESI_protocol::do_snoop_S (Mreq *request)
{
	switch (request->msg) {
		case GETS:
			set_shared_line();
		case DATA:
			break;
		case GETM:
			state = MESI_CACHE_I;
			break;
		default:
			request->print_msg (my_table->moduleID, "ERROR");
			fatal_error ("Client: S state shouldn't see this message\n");
	}
}


/*
 * Handles sending data to other caches and updating state based on 
 * GETM or GETS
 */
inline void MESI_protocol::do_snoop_E (Mreq *request)
{
	//printf("Something's about to leave E! %lu mreq: \n", Global_Clock);
	switch (request->msg) {
		case GETS:
			set_shared_line();
			send_DATA_on_bus(request->addr,request->src_mid);
			state = MESI_CACHE_S;	
			break;
		case GETM:
			set_shared_line();
			send_DATA_on_bus(request->addr,request->src_mid);
			state = MESI_CACHE_I;
			break;
		case DATA:
			fatal_error ("Should not see data/mreq_invalid for this line!"  
					"I have the line!");
			break;
		default:
			request->print_msg (my_table->moduleID, "ERROR");
			fatal_error ("Client: E state shouldn't see this message\n");
	}
}

/*
 * Handles sending data to other caches. E and M are exactly the same ehre
 */
inline void MESI_protocol::do_snoop_M (Mreq *request)
{
	do_snoop_E(request);
}

/*
 * Handles waiting for data to be received from GETM
 */
inline void MESI_protocol::do_snoop_IM (Mreq *request)
{
	switch (request->msg) {
		case GETS: //wut why
		case GETM: //my own, do nuthin
			break;
		case DATA:
			send_DATA_to_proc(request->addr);
			state = MESI_CACHE_M;
			break;
		default:
			request->print_msg (my_table->moduleID, "ERROR");
			fatal_error ("Client: IM state shouldn't see this message\n");
	}
}

/*
 * Handles waiting on data to grab it's own data from S->M while still 
 * acting like in S state
 */
inline void MESI_protocol::do_snoop_SM (Mreq *request)
{
	switch (request->msg) {
		case GETS: //wut why
			set_shared_line();
		case GETM: //my own, do nuthin
			break;
		case DATA:
			send_DATA_to_proc(request->addr);
			state = MESI_CACHE_M;
			break;
		default:
			request->print_msg (my_table->moduleID, "ERROR");
			fatal_error ("Client: SM state shouldn't see this message\n");
	}
}
