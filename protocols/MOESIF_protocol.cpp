#include "MOESIF_protocol.h"
#include "../sim/mreq.h"
#include "../sim/sim.h"
#include "../sim/hash_table.h"
#define D (0)

extern Simulator *Sim;

/*************************
 * Constructor/Destructor.
 *************************/
MOESIF_protocol::MOESIF_protocol (Hash_table *my_table, Hash_entry *my_entry)
    : Protocol (my_table, my_entry)
{
	this->state = MOESIF_CACHE_I;
}

MOESIF_protocol::~MOESIF_protocol ()
{    
}

void MOESIF_protocol::dump (void)
{
    const char *block_states[7] = {"X","I","S","E","O","M","F"};
    fprintf (stderr, "MOESIF_protocol - state: %s\n", block_states[state]);
}

/*
 * Handle proc request for data based on current state. Make requests
 * to BUS if necessary and handle any state update according to current state
 */
void MOESIF_protocol::process_cache_request (Mreq *request)
{
	switch (state) {
		case MOESIF_CACHE_I:  if(D)printf("cI\n");do_cache_I (request); break; 
		case MOESIF_CACHE_IS:  if(D)printf("cIS\n");do_cache_IT (request); break;
		case MOESIF_CACHE_S:  if(D)printf("cS\n");do_cache_S (request); break;
		case MOESIF_CACHE_M:  if(D)printf("cM\n");do_cache_M (request); break;
		case MOESIF_CACHE_IM: if(D)printf("cIM\n");do_cache_IT (request); break;
		case MOESIF_CACHE_O: if(D)printf("cO\n");do_cache_O (request); break;
		case MOESIF_CACHE_OM: if(D)printf("cOM\n");do_cache_IT (request); break;
		case MOESIF_CACHE_SM: if(D)printf("cOM\n");do_cache_IT (request); break;
		case MOESIF_CACHE_E: if(D)printf("cE\n");do_cache_E (request); break;
		case MOESIF_CACHE_F: if(D)printf("cF\n");do_cache_F (request); break;
    default:
        fatal_error ("Invalid Cache State for MOESIF Protocol\n");
    }
}

/*
 * Based on current state, call function associated with state
 * and perform any snooping of BUS or state updates required by this state
 * based on bus information
 */
void MOESIF_protocol::process_snoop_request (Mreq *request)
{
	switch (state) {
		case MOESIF_CACHE_I: if(D)printf("sI\n"); do_snoop_I (request); break;
		case MOESIF_CACHE_IS: if(D)printf("sIS\n"); do_snoop_IS (request); break;
		case MOESIF_CACHE_S:  if(D)printf("sS\n");do_snoop_S (request); break;
		case MOESIF_CACHE_M:  if(D)printf("sM\n");do_snoop_M (request); break;
		case MOESIF_CACHE_IM: if(D)printf("sIM\n");do_snoop_IM (request); break;
		case MOESIF_CACHE_O: if(D)printf("sO\n");do_snoop_O (request); break;
		case MOESIF_CACHE_OM: if(D)printf("sOM\n");do_snoop_OM (request); break;
		case MOESIF_CACHE_SM: if(D)printf("sOM\n");do_snoop_SM (request); break;
		case MOESIF_CACHE_E: if(D)printf("sE\n");do_snoop_E (request); break;
		case MOESIF_CACHE_F: if(D)printf("sF\n");do_snoop_F (request); break;
    default:
    	fatal_error ("Invalid Cache State for MOESIF Protocol\n");
    }
}

/*
 * We're invalid, so request the data from BUS no matter what
 */
inline void MOESIF_protocol::do_cache_I (Mreq *request)
{
	switch (request->msg) {
		case LOAD:
			send_GETS(request->addr);
			state = MOESIF_CACHE_IS;
			Sim->cache_misses++;
			break;
		case STORE:
			send_GETM(request->addr);
			state = MOESIF_CACHE_IM;
			Sim->cache_misses++;
			break;
		default:
			request->print_msg (my_table->moduleID, "ERROR");
			fatal_error ("Client: I state shouldn't see this message\n");
	}
}

/*Dummy do_cache taht should never happen since I should be waiting on 
 * a response from BUS first
 */
inline void MOESIF_protocol::do_cache_IT (Mreq *request)
{
	switch (request->msg) {
		case LOAD:
		case STORE:
			request->print_msg (my_table->moduleID, "ERROR");
			fatal_error("Should only have one outstanding request per processor!");
		default:
			request->print_msg (my_table->moduleID, "ERROR");
			fatal_error ("Client: I state shouldn't see this message\n");
	}
}

/*
 * Supply data on read. On write, GETM and then 
 * wait until you see GETM in SM state to supply data to 
 * self. Must do this to let other caches know you
 * modified data
 */
inline void MOESIF_protocol::do_cache_S (Mreq *request)
{
	switch (request->msg) {
		case STORE:
			//Invalidate other caches
			send_GETM(request->addr);
			state = MOESIF_CACHE_SM;
			Sim->cache_misses++;
			break;
		case LOAD:
			send_DATA_to_proc(request->addr);
			break;
		default:
			request->print_msg (my_table->moduleID, "ERROR");
			fatal_error ("Client: M state shouldn't see this message\n");
	}
}

/*
 * Perform silent upgrade on write or just supply data to proc on read
 */
inline void MOESIF_protocol::do_cache_E (Mreq *request)
{
	switch (request->msg) {
		case STORE:
			Sim->silent_upgrades++;
			state = MOESIF_CACHE_M;
		case LOAD:
			send_DATA_to_proc(request->addr);
			break;
		default:
			request->print_msg (my_table->moduleID, "ERROR");
			fatal_error ("Client: E state shouldn't see this message\n");
	}
}

/*
 * On load, just supply data
 * On store, broadcast GETM to other caches and attempt to suppply data to self by
 * moving to OM
 */
inline void MOESIF_protocol::do_cache_F (Mreq *request)
{
	switch (request->msg) {
		case STORE:
			//Sim->silent_upgrades++;
			state = MOESIF_CACHE_OM;
			send_GETM(request->addr);
			Sim->cache_misses++;
			break;
		case LOAD:
			send_DATA_to_proc(request->addr);
			break;
		default:
			request->print_msg (my_table->moduleID, "ERROR");
			fatal_error ("Client: F state shouldn't see this message\n");
	}
}

/*
 * On STORE, GETM to alert other caches of modification and supply data to self
 * On LOAD, just send data to proc
 */
inline void MOESIF_protocol::do_cache_O (Mreq *request)
{
	switch (request->msg) {
		case STORE:
			//sleep(1);
			send_GETM(request->addr);
			state = MOESIF_CACHE_OM;
			Sim->cache_misses++;
			break;
		case LOAD:
			send_DATA_to_proc(request->addr);
			break;
		default:
			request->print_msg (my_table->moduleID, "ERROR");
			fatal_error ("Client: O state shouldn't see this message\n");
	}
}

/*Since we're in M, supply data to proc no matter what.
 * WE dah boss.
 */
inline void MOESIF_protocol::do_cache_M (Mreq *request)
{
	switch (request->msg) {
		case LOAD:
		case STORE:
			send_DATA_to_proc(request->addr);
			break;
		default:
			request->print_msg (my_table->moduleID, "ERROR");
			fatal_error ("Client: M state shouldn't see this message\n");
	}
}

/*
 * Snoops for I and does nothing, since it's I.
 * Boring snoop
 */
inline void MOESIF_protocol::do_snoop_I (Mreq *request)
{
	switch (request->msg) {
		case GETS:
		case GETM:
		case DATA:
			break;
		default:
			request->print_msg (my_table->moduleID, "ERROR");
			fatal_error ("Client: I state shouldn't see this message\n");
	}
}

/*Handles getting data from GETS on invalid cache line
 */
inline void MOESIF_protocol::do_snoop_IS (Mreq *request)
{
	switch (request->msg) {
		case GETM: //WUT WHY
		case GETS:
			break; //MY OWN
		case DATA:
			if (!get_shared_line())
				state = MOESIF_CACHE_E;
			else
				state = MOESIF_CACHE_S;
			set_shared_line();
			send_DATA_to_proc(request->addr);
			break;
		default:
			request->print_msg (my_table->moduleID, "ERROR");
			fatal_error ("Client: IS state shouldn't see this message\n");
	}
}

/*
 * Checks line and sets that shared to let others now a cache does have this
 * data
 * Invalidates on GETM
 */
inline void MOESIF_protocol::do_snoop_S (Mreq *request)
{
	switch (request->msg) {
		case GETS:
			set_shared_line();
		case DATA:
			break;
		case GETM:
			state = MOESIF_CACHE_I;
			break;
		default:
			request->print_msg (my_table->moduleID, "ERROR");
			fatal_error ("Client: S state shouldn't see this message\n");
	}
}

/*
 * Always supply data, but invalidate on GETM and move to F on GETS
 */
inline void MOESIF_protocol::do_snoop_E (Mreq *request)
{
	switch (request->msg) {
		case GETS:
			set_shared_line();
			send_DATA_on_bus(request->addr,request->src_mid);
			state = MOESIF_CACHE_F;	
			break;
		case GETM:
			set_shared_line();
			send_DATA_on_bus(request->addr,request->src_mid);
			state = MOESIF_CACHE_I;
			break;
		case DATA:
			fatal_error ("Should not see data/mreq_invalid for this line!"  
					"I have the line!");
			break;
		default:
			request->print_msg (my_table->moduleID, "ERROR");
			fatal_error ("Client: E state shouldn't see this message\n");
	}
}

/*
 * Always supply data and update state to I if GETM is seen
 */
inline void MOESIF_protocol::do_snoop_F (Mreq *request)
{
	switch (request->msg) {
		case GETS:
			set_shared_line();
			send_DATA_on_bus(request->addr,request->src_mid);
			break;
		case GETM:
			set_shared_line();
			send_DATA_on_bus(request->addr,request->src_mid);
			state = MOESIF_CACHE_I;
			break;
		case DATA:
			fatal_error ("Should not see data/mreq_invalid for this line!"  
					"I have the line!");
			break;
		default:
			request->print_msg (my_table->moduleID, "ERROR");
			fatal_error ("Client: F state shouldn't see this message\n");
	}
}

/*
 * Supply data in all cases and invalidate if see a GETM
 */
inline void MOESIF_protocol::do_snoop_O (Mreq *request)
{
	switch (request->msg) {
		case GETS:
			set_shared_line();
			send_DATA_on_bus(request->addr,request->src_mid);
			break;
		case GETM:
			if(!get_shared_line()){
				set_shared_line();
				send_DATA_on_bus(request->addr,request->src_mid);
			}
			state = MOESIF_CACHE_I;
			break;
		case DATA:
			fatal_error ("Should not see data/mreq_invalid for this line!"  
					"I have the line!");
			break; default: request->print_msg (my_table->moduleID, "ERROR"); fatal_error ("Client: M state shouldn't see this message\n"); }
}

/*
 * Handles sending data to other caches and updating state based on 
 * GETM or GETS
 */
inline void MOESIF_protocol::do_snoop_M (Mreq *request)
{
	switch (request->msg) {
		case GETS:
			set_shared_line();
			send_DATA_on_bus(request->addr,request->src_mid);
			state = MOESIF_CACHE_O;
			break;
		case GETM:
			if(!get_shared_line()){
				set_shared_line();
				send_DATA_on_bus(request->addr,request->src_mid);
			}
			state = MOESIF_CACHE_I;
			break;
		case DATA:
			fatal_error ("Should not see data/mreq_invalid for this line!"  
					"I have the line!");
			break;
		default:
			request->print_msg (my_table->moduleID, "ERROR");
			fatal_error ("Client: M state shouldn't see this message\n");
	}
}

/*
 * Handles waiting for data to be received from GETM
 */
inline void MOESIF_protocol::do_snoop_IM (Mreq *request)
{
	switch (request->msg) {
		case GETS: //wut why
		case GETM: //my own, do nuthin
			break;
		case DATA:
			send_DATA_to_proc(request->addr);
			state = MOESIF_CACHE_M;
			break;
		default:
			request->print_msg (my_table->moduleID, "ERROR");
			fatal_error ("Client: IM state shouldn't see this message\n");
	}
}

/*
 * Handle receiving data from supplier and moving to M
 * Still act as O and supply data when necessary
 */
inline void MOESIF_protocol::do_snoop_OM (Mreq *request)
{
	switch (request->msg) {
		case DATA:
			send_DATA_to_proc(request->addr);
			state = MOESIF_CACHE_M;
			break;
		case GETS:
		case GETM:
			if(!get_shared_line()){
				send_DATA_on_bus(request->addr,request->src_mid);
			}
			set_shared_line();
			break;
		default:
			request->print_msg (my_table->moduleID, "ERROR");
			fatal_error ("Client: OM state shouldn't see this message\n");
	}
}

/*
 * Receive data to move to M state (typically from self), but still act as if in
 * S state until data is received
 */
inline void MOESIF_protocol::do_snoop_SM (Mreq *request)
{
	switch (request->msg) {
		case GETS: //wut why
			set_shared_line();
		case GETM: //my own, do nuthin
			break;
		case DATA:
			send_DATA_to_proc(request->addr);
			state = MOESIF_CACHE_M;
			break;
		default:
			request->print_msg (my_table->moduleID, "ERROR");
			fatal_error ("Client: IM state shouldn't see this message\n");
	}
}
